#--- make windows key as mod/main key
set $mod Mod4
set $editor vim
set $term st


#--- make alt key as mod/main key
# set $mod Mod1

#custom system colors

set_from_resource $darkred     color1  #000000
set_from_resource $red         color9  #000000
set_from_resource $darkgreen   color2  #000000
set_from_resource $green       color10 #000000
set_from_resource $darkyellow  color3  #000000
set_from_resource $yellow      color11 #000000
set_from_resource $darkblue    color4  #000000
set_from_resource $blue        color12 #000000
set_from_resource $darkmagenta color5  #000000
set_from_resource $magenta     color13 #000000
set_from_resource $darkcyan    color6  #000000
set_from_resource $cyan        color14 #000000
set_from_resource $darkwhite   color7  #000000
set_from_resource $white       color15 #000000

#--- custom black colors ---
set $black       #282828
set $darkblack   #1d2021
set $transparent #00000000

# custom window color codes
set $yellow	#b58900
set $orange	#cb4b16
set $red	#ff0000
set $magenta #d33682
set $violet	#6c71c4
set $blue	#1E90FF
set $cyan	#2aa198
set $green	#32CD32
set $white	#ffffff

set $height 34

workspace_auto_back_and_forth yes
force_display_urgency_hint 0 ms
focus_on_window_activation urgent


floating_minimum_size -1 x -1
floating_maximum_size -1 x -1

#--- make browser pop-ups float
for_window [window_role="pop-up"] floating enable
for_window [class="Ranger" instance="ranger"] floating enable
for_window [class="Cmus" instance="cmus"] floating enable

#--- global font ---
font pango:Apercu Mono Pro, Bold 9

#--- drag floating windows using Mouse + Mod ---
floating_modifier $mod

#--- start a terminal ---
bindsym $mod+Return exec terminator
for_window [class="Terminator"] resize set 600 400
for_window [class="Ranger"] resize set 600 400
for_window [class="cmus"] resize set 400 200

#--- kill focused window ---
bindsym $mod+Shift+q kill

#--- start dmenu launcher ---
# bindsym $mod+d exec dmenu_run

set $execute exec rofi -show run -eh 1 -padding 5 -font "Apercu Mono Pro 14"
bindsym $mod+d $execute
bindsym XF86Search $execute
bindsym $mod+shift+d exec rofi -show run -eh 1 -padding 5 -font "Apercu Mono Pro 14"

#--- set scaling 100
exec xrandr --dpi 100

#--- change window focus ---
bindsym $mod+j focus left
bindsym $mod+k focus down
bindsym $mod+l focus up
bindsym $mod+ae focus right

bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

#--- move focused window ---
bindsym $mod+Shift+j move left
bindsym $mod+Shift+k move down
bindsym $mod+Shift+l move up
bindsym $mod+Shift+ae move right

bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

#--- split in horizontal orientation ---
bindsym $mod+h split h

#--- split in vertical orientation ---
bindsym $mod+v split v

#--- make selected window fullscreen ---
bindsym $mod+f fullscreen toggle

#--- change container layout ---
bindsym $mod+s layout stacking
bindsym $mod+t layout tabbed
bindsym $mod+e layout toggle split

#--- switch between tiling and floating ---
bindsym $mod+Shift+space floating toggle

#---toggle focus between floating and tiling windows ---
bindsym $mod+space focus mode_toggle

#--- focus the parent container ---
bindsym $mod+a focus parent

#--- workspace names ---
set $workspace0 "0: IRC"
set $workspace1 "1: FIREFOX"
set $workspace2 "2: TERM"
set $workspace3 "3: FILES"
set $workspace4 "4: EDITOR"
set $workspace5 "5: SOCIAL"
set $workspace6 "6: SERVERS"

#--- switch to workspace ---
bindsym $mod+0 workspace $workspace0
bindsym $mod+1 workspace $workspace1
bindsym $mod+2 workspace $workspace2
bindsym $mod+3 workspace $workspace3
bindsym $mod+4 workspace $workspace4
bindsym $mod+5 workspace $workspace5
bindsym $mod+6 workspace $workspace6

#--- move focused container to workspace ---
bindsym $mod+Shift+0 move container to workspace $workspace0
bindsym $mod+Shift+1 move container to workspace $workspace1
bindsym $mod+Shift+2 move container to workspace $workspace2
bindsym $mod+Shift+3 move container to workspace $workspace3
bindsym $mod+Shift+4 move container to workspace $workspace4
bindsym $mod+Shift+5 move container to workspace $workspace5
bindsym $mod+Shift+6 move container to workspace $workspace6

#--- reload the configuration file ---
bindsym $mod+Shift+c reload
#--- restart i3 inplace ---
bindsym $mod+Shift+r restart

#--- exit i3 ---
#bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'Dont Stop the X Server' -b 'Yes, exit i3' 'i3-msg exit'"

#--- resize window ---
mode "resize" {

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym j resize shrink width 5 px or 5 ppt
        bindsym k resize grow height 5 px or 5 ppt
        bindsym l resize shrink height 5px or 5 ppt
        bindsym ae resize grow width 5 px or 5 ppt

        bindsym Left resize shrink width 5 px or 5 ppt
        bindsym Down resize grow height 5 px or 5 ppt
        bindsym Up resize shrink height 5 px or 5 ppt
        bindsym Right resize grow width 5 px or 5 ppt

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

bindsym $mod+r mode "resize"

bindsym --release Caps_Lock exec pkill -SIGRTMIN+11 i3blocks
bindsym --release Num_Lock  exec pkill -SIGRTMIN+11 i3blocks


#--- window themes ---
# option               border  background   text   indicator
client.focused 			$green	$green	$green $white
client.focused_inactive	$grey $grey $grey $grey
client.unfocused  		$grey $grey $grey $grey
client.urgent 	 		$red $red $red $red

#--- i3 status bar themes ---
bar {
        position bottom
        colors {
        background $black
        statusline #ffffff
        separator  #ff0000

        focused_workspace  $green $transparent $green
        active_workspace   $white $transparent $white
        urgent_workspace   $red $transparent $red
    }
		status_command i3status -c /home/username/.i3status.conf
}

#--- lock computer ---
bindsym $mod+Shift+x exec i3lock --color aaaaaa

#--- screenshot ---
bindsym --release Print exec --no-startup-id "xfce4-screenshooter $HOME/Pictures/screenshot-$(date +%Y-%m-%d_%H-%M-%S).png"
bindsym --release Shift+Print exec --no-startup-id "xfce4-screenshooter $HOME/Pictures/screenshot-$(date +%Y-%m-%d_%H-%M-%S).png"
bindsym --release $mod+Shift+s exec "import png:- | imgur | tee -a $HOME/Pictures/imgur_log/screenshot_log.txt && cat screenshot_log.txt | tail -n 1 | xclip -i"

#bindsym --release Print exec "scrot -m '/home/username/Pictures/screenshots/%s_%H%M_%d.%m.%Y_$wx$h.png'"
#bindsym --release Shift+Print exec "scrot -s '/home/username/Pictures/screenshots/%s_%H%M_%d%m%Y_$wx$h.png'"
#bindsym --release $mod+Shift+Print exec "scrot -u -d 4 '/home/username/Pictures/screenshots/%s_%H%M_%d%m%Y_$wx$h.png'"

# Cycle through active workspaces
bindsym $mod+Tab workspace next
bindsym $mod+Shift+Tab workspace prev


bindsym $mod+n exec --no-startup-id pcmanfm
bindsym $mod+w exec --no-startup-id firefox
bindsym $mod+g exec --no-startup-id geany
bindsym $mod+Shift+Return exec st
bindsym $mod+Shift+r exec $term -e ranger
bindsym $mod+Shift+m exec $term -e cmus
bindsym $mod+Shift+n exec $term -e /home/username/newsboat


#--- pulse/alsa audio controls ---
bindsym XF86AudioRaiseVolume exec pactl set-sink-volume @DEFAULT_SINK@ +2%; exec pactl set-sink-mute @DEFAULT_SINK@ 0
bindsym XF86AudioLowerVolume exec pactl set-sink-volume @DEFAULT_SINK@ -2%; exec pactl set-sink-mute @DEFAULT_SINK@ 0
bindsym XF86AudioMute exec pactl set-sink-mute @DEFAULT_SINK@ toggle
bindsym Control+Up exec amixer -D pulse sset Master 10%+
bindsym Control+Down exec amixer -D pulse sset Master 10%-

#--- media player controls ---
bindsym XF86AudioPlay exec playerctl play-pause
bindsym XF86AudioNext exec playerctl next
bindsym XF86AudioPrev exec playerctl previous

#--- sreen brightness controls ---
bindsym XF86MonBrightnessUp exec xbacklight -inc 10 # increase screen brightness
bindsym XF86MonBrightnessDown exec xbacklight -dec 10 # decrease screen brightness

#--- touchpad controls ---

exec --no-startup-id synclient VertEdgeScroll=50 HorizEdgeScroll=50
exec --no-startup-id synclient VertTwoFingerScroll=1 HorizTwoFingerScroll=1
exec --no-startup-id synclient VertScrollDelta=-100 HorizScrollDelta=-100
exec --no-startup-id synclient NaturalScrolling=0
exec --no-startup-id synclient MaxTapTime=0

#--- remove title bar ---
new_window pixel 5

for_window [class="^.*"] border pixel 2
new_window 1pixel

#--- assign permanent windows to programs ---
assign [class="Hexchat"] 0: IRC
assign [class="Firefox"] 1: FIREFOX
assign [class="Terminator"] 2: TERM
assign [class="Thunar"] 3: FILES
assign [class="Geany"] 4: EDITOR
assign [class="Flowblade"] 4: EDITOR
assign [class="TelegramDesktop"] 5: SOCIAL
assign [class="Discord"] 5: SOCIAL
assign [class="Vncviewer"] 6: SERVERS
assign [class="Vncserver"] 6: SERVERS
assign [class="Deluge"] 6: SERVERS

#--- set wallpaper ---
#exec_always --no-startup-id feh --bg-max ~/1.jpg

#--- startup programs ---
exec --no-startup-id /usr/lib/policykit-1-gnome/polkit-gnome-authentication-agent-1 &
exec --no-startup-id compton -b
exec --no-startup-id nm-applet
exec --no-startup-id synclient
exec --no-startup-id clipit
exec --no-startup-id vncserver-x11-serviced
exec --no-startup-id vncviewer
exec --no-startup-id xset s off
exec --no-startup-id xset -dpms
exec --no-startup-id xset s noblank

exec --no-startup-id i3-msg 'workspace 1: FIREFOX; exec firefox'
exec --no-startup-id i3-msg 'workspace 2: TERM; exec $term'
exec --no-startup-id i3-msg 'workspace 3: FILES; exec $term -e ranger'
exec --no-startup-id i3-msg 'workspace 4: EDITOR; exec geany'
exec --no-startup-id i3-msg 'workspace 5: SOCIAL; exec /home/username/Telegram'
exec --no-startup-id i3-msg 'workspace 5: SOCIAL; exec discord'
exec --no-startup-id i3-msg 'workspace 6: SERVERS; vncviewer